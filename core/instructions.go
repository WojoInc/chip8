package core

import (
	"fmt"
	"math/rand"
)

// The following functions each implement a portion of the Instruction set,
// broken up based on the upper nibble of the first byte of each opcode.
// This allows the Execute() method to more easily choose the correct function
// for a given opcode by simply masking off the most significant nibble

// opGrp0 implements the instructions for which the upper 4 bits are 0
func (cpu *CPU) opGrp0(opr Operands) {

	// Skip checking debug flag, as below logic is required regardless
	switch opr {
	case 0x0E0:
		//cpu.displayCon.Command <- DisplayMessage{Operation: DISPLAY_CLEAR}
		cpu.system.Display.clear()
	case 0x0EE:
		// Return from subroutine by popping PC off of stack
		cpu.pc = cpu.stack.Pop()
	default:
		// If we end up here, this is an invalid opcode
		panic(fmt.Sprintf("Invalid opcode: %#X", opr))
	}
	return
}

// opGrp1 implements the instructions for which the upper 4 bits are 1
// There is only 1 Instruction here.
// This method performs a jump, setting the program counter to the address
// given by the lower 12 bits of the opcode
func (cpu *CPU) opGrp1(opr Operands) {
	// Check that address is within appropriate range for Instruction storage
	if 0x200 <= opr && opr <= 0xE8F {
		cpu.pc = Word(opr)
		return
	}
	panic(fmt.Sprintf("Attempt to jump to invalid location: %#X", opr))
}

// opGrp2 implements the instructions for which the upper 4 bits are 2
func (cpu *CPU) opGrp2(opr Operands) {
	// Push pc to stack
	if err := cpu.stack.Push(cpu.pc); err != nil {
		// Panic if we've exceeded subroutine depth based on StackSz
		panic(fmt.Sprintf("Failed to call subroutine: %s", err.Error()))
	}
	cpu.pc = Word(opr)
}

// opGrp3 implements the instructions for which the upper 4 bits are 3
// There is only 1 opcode here.
// This method increments the program counter (skipping the next Instruction)
// if VX == NN, where X is the lower nibble of the first byte, and NN is the second byte
func (cpu *CPU) opGrp3(opr Operands) {
	if cpu.V[opr>>8] == byte(opr&0xFF) {
		cpu.pc += 2
	}
}

// opGrp4 implements the instructions for which the upper 4 bits are 4
// There is only 1 opcode here.
// This method increments the program counter (skipping the next Instruction)
// if VX != NN, where X is the lower nibble of the first byte, and NN is the second byte
func (cpu *CPU) opGrp4(opr Operands) {
	if cpu.V[opr>>8] != byte(opr&0xFF) {
		cpu.pc += 2
	}
}

// opGrp5 implements the instructions for which the upper 4 bits are 5
// There is only 1 opcode here.
// This method increments the program counter (skipping the next Instruction)
// if VX == VY, where X and Y are bits 12-4 of the opcode
func (cpu *CPU) opGrp5(opr Operands) {
	if cpu.V[opr>>8] == cpu.V[opr>>4&0x0F] {
		cpu.pc += 2
	}
}

// opGrp6 implements the instructions for which the upper 4 bits are 6
// There is only 1 opcode here. This method stores the value of the second
// byte of the opcode into the register given by the lower nibble of the first byte
func (cpu *CPU) opGrp6(opr Operands) {
	cpu.V[opr>>8] = byte(opr & 0xFF)
}

// opGrp7 implements the instructions for which the upper 4 bits are 7
// There is only 1 opcode here. This method adds the value of the second byte to the
// register given by the lower nibble of the first byte
// The carry flag is not modified
func (cpu *CPU) opGrp7(opr Operands) {
	cpu.V[opr>>8] += byte(opr & 0xFF)
}

// opGrp8 implements the instructions for which the upper 4 bits are 8
// This method covers multiple arithmetic and bitwise functions originally
// delegated to the ALU of some physical implementations
// Registers are given by the lower nibble of the first byte and upper nibble of the second.
// Lower nibble of the second byte determines the actual operation performed
func (cpu *CPU) opGrp8(opr Operands) {
	rX := opr >> 8
	rY := opr >> 4 & 0x0F
	switch opr & 0xF {
	case 0x0:
		cpu.V[rX] = cpu.V[rY]
	case 0x1:
		cpu.V[rX] |= cpu.V[rY]
	case 0x2:
		cpu.V[rX] &= cpu.V[rY]
	case 0x3:
		cpu.V[rX] ^= cpu.V[rY]
	case 0x4:
		// Use an int to check overflow
		tmp := int(cpu.V[rX]) + int(cpu.V[rY])
		if tmp > 255 {
			cpu.V[rX] = byte(tmp % 256)
			cpu.V[0xF] = 0x1
		} else {
			cpu.V[rX] = byte(tmp)
			cpu.V[0xF] = 0x0
		}
	case 0x5:
		tmp := int(cpu.V[rX]) - int(cpu.V[rY])
		if tmp < 0 {
			cpu.V[rX] = byte(tmp % 256)
			cpu.V[0xF] = 0x0
		} else {
			cpu.V[rX] = byte(tmp)
			cpu.V[0xF] = 0x1
		}
	case 0x6:
		// Handle case where VF is supplied as VX
		lsb := cpu.V[rY] & 0x1
		cpu.V[rX] = cpu.V[rY] >> 1
		cpu.V[0xF] = lsb
	case 0x7:
		tmp := int(cpu.V[rY]) - int(cpu.V[rX])
		if tmp < 0 {
			cpu.V[rX] = byte(tmp % 256)
			cpu.V[0xF] = 0x0
		} else {
			cpu.V[rX] = byte(tmp)
			cpu.V[0xF] = 0x1
		}
	case 0xE:
		// Handle case where VF is supplied as VX
		msb := cpu.V[rX] & 0x80 >> 7
		cpu.V[rX] = cpu.V[rX] << 1
		cpu.V[0xF] = msb
	default:
		panic(fmt.Sprintf("Unknown opcode: 0x8%X", opr))
	}
}

func (cpu *CPU) opGrp9(opr Operands) {
	if opr&0xF != 0 {
		panic(fmt.Sprintf("Unknown opcode: 0x9%X", opr))
	}
	if cpu.V[opr>>8] != cpu.V[opr>>4&0xF] {
		cpu.pc += 2
	}
}

func (cpu *CPU) opGrpA(opr Operands) {
	cpu.I = Word(opr)
}

func (cpu *CPU) opGrpB(opr Operands) {
	loc := Word(cpu.V[0x0]) + Word(opr)
	if 0x200 <= loc && loc <= 0xE8F {
		cpu.pc = Word(cpu.V[0x0]) + Word(opr)
		return
	}
	panic(fmt.Sprintf("Attempt to jump to invalid address: %#X", loc))
}

func (cpu *CPU) opGrpC(opr Operands) {
	cpu.V[opr>>8] = byte(rand.Intn(255)) & byte(opr&0xFF)
}

func (cpu *CPU) opGrpD(opr Operands) {
	x := cpu.V[opr>>8]
	y := cpu.V[opr>>4&0xF]
	n := byte(opr & 0xF)
	var i byte
	sprite := make([]byte, n)
	for i = 0; i < n; i++ {
		sprite[i] = cpu.system.ram.read(cpu.I + Word(i))
	}

	cpu.V[0xF] = func(b bool) byte {
		if b {
			return 0x1
		}
		return 0x0
	}(cpu.system.Display.drawSprite(int(x), int(y), sprite))
}

func (cpu *CPU) opGrpE(opr Operands) {
	switch opr & 0xFF {
	case 0x9E:
		if cpu.system.Keypad.getKey(cpu.V[opr>>8]) {
			cpu.pc += 2
		}
	case 0xA1:
		if !cpu.system.Keypad.getKey(cpu.V[opr>>8]) {
			cpu.pc += 2
		}
	default:
		panic("")
	}
}

func (cpu *CPU) opGrpF(opr Operands) {
	switch opr & 0xFF {
	case 0x07:
		cpu.V[opr>>8] = cpu.system.Delay.value
	case 0x0A:
		// Note, this method prevents CPU from resetting while awaiting keypress
		//cpu.V[opr>>8] = cpu.system.Keypad.await()
		// More efficient method using blocking channel to avoid
		// wasting CPU on for loop
		await := make(chan byte)
		cpu.system.Keypad.await(await)
		cpu.V[opr>>8] = <-await
	case 0x15:
		cpu.system.Delay.value = cpu.V[opr>>8]
	case 0x18:
		cpu.system.Sound.value = cpu.V[opr>>8]
	case 0x1E:
		cpu.I += Word(cpu.V[opr>>8])
	case 0x29:
		cpu.I = CharAddress(cpu.V[opr>>8])
	case 0x33:
		val := toBcd(cpu.V[opr>>8])
		cpu.system.ram.write(cpu.I, byte(val>>8))
		cpu.system.ram.write(cpu.I+1, byte(val>>4&0xF))
		cpu.system.ram.write(cpu.I+2, byte(val&0xF))
	case 0x55:
		for i := 0; i <= int(opr>>8); i++ {
			cpu.system.ram.write(cpu.I+Word(i), cpu.V[i])
		}
	case 0x65:
		for i := 0; i <= int(opr>>8); i++ {
			cpu.V[i] = cpu.system.ram.read(cpu.I + Word(i))
		}
	default:
		panic("")
	}
}
