package core

// Word simply masks uint16 to provide a more convenient name
// when reading code later
type Word uint16

// Operands masks uint16. It's different from Word, in that its intended
// to indicate a 12bit value.
type Operands uint16

// Instruction defines the type for any functions which represent CPU instructions
// Not strictly necessary, but helps with readability
type Instruction func(opr Operands)
