package core

type SoundTimer struct {
	value    byte
	beepHook func(bool)
}

func (s *SoundTimer) RegisterHook(hook func(bool)) {
	s.beepHook = hook
}

func (s *SoundTimer) tick() {
	// If sound device is attached
	if s.beepHook != nil {
		// If we've hit 0x01 or lower, stop sound device playback
		if s.value < 0x02 {
			s.beepHook(false)
		} else {
			s.beepHook(true)
		}
	}
	if s.value <= 0 {
		s.value = 0
	} else {
		s.value -= 1
	}

}

type DelayTimer struct {
	value byte
}

func (d *DelayTimer) tick() {
	if d.value <= 0 {
		d.value = 0
	} else {
		d.value -= 1
	}
}
