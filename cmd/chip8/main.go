package main

import (
	"flag"
	"fmt"
	"github.com/wojoinc/chip8/core"
	"github.com/wojoinc/chip8/ui"
)

var (
	system *core.System
)

func main() {
	system = core.NewCHIP8System()
	flag.Parse()
	romFile := flag.Arg(0)
	r := core.ROM{FilePath: romFile}
	if err := r.Load(); err != nil {
		fmt.Println(err.Error())
		return
	}

	// Load ROM into system memory
	system.ROMtoRAM(r.GetBytes())

	ui.RunUI(system)
	system.TearDown()
}
