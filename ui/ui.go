package ui

import (
	"fmt"
	rgui "github.com/gen2brain/raylib-go/raygui"
	rl "github.com/gen2brain/raylib-go/raylib"
	"github.com/wojoinc/chip8/core"
	"image/color"
)

var (
	keymap = map[int32]int{
		rl.KeyOne: 0x1, rl.KeyTwo: 0x2, rl.KeyThree: 0x3, rl.KeyFour: 0xC,
		rl.KeyQ: 0x4, rl.KeyW: 0x5, rl.KeyE: 0x6, rl.KeyR: 0xD,
		rl.KeyA: 0x7, rl.KeyS: 0x8, rl.KeyD: 0x9, rl.KeyF: 0xE,
		rl.KeyZ: 0xA, rl.KeyX: 0x0, rl.KeyC: 0xB, rl.KeyV: 0xF,
	}
	scalingFactor = 10
	pixels        []bool
	playBtn       bool
	soundBtn      bool
	System        *core.System
)

func RunUI(system *core.System) {
	System = system
	rl.InitWindow(1366, 768, "CHIP-8 Emulator")
	initAudio()
	defer rl.CloseWindow()
	rl.SetTargetFPS(60)
	pixels = System.Display.GetPixels()

	for !rl.WindowShouldClose() {
		update()
		draw()
	}
}

func draw() {
	rl.BeginDrawing()
	rl.ClearBackground(rl.Black)
	drawDisplay(pixels)
	drawRegisters()
	playBtn = rgui.Button(rl.NewRectangle(0, 700, 100, 30), "Play/Pause")
	soundBtn = rgui.Button(rl.NewRectangle(300, 700, 100, 30), "Beeper")

	rl.EndDrawing()
}

func drawRegisters() {
	text := getRegisterText()
	rgui.TextBox(rl.NewRectangle(700, 0, 100, 400), &text, 20, false)
}

func getRegisterText() string {
	output := "Registers:\n"
	for i, v := range System.CPU.V {
		output += fmt.Sprintf("V%X: 0x%.2X\n", i, v)
	}
	return output
}

func update() {
	checkKeys()
	if playBtn {
		System.ToggleRunning()
	}
	if soundBtn {
		if playing {
			rl.StopAudioStream(as)
		} else {
			rl.PlayAudioStream(as)
		}
		playing = !playing
	}
}

func drawDisplay(pixels []bool) {
	for i := 0; i < core.DisplayH; i++ {
		for j := 0; j < core.DisplayW; j++ {
			drawPixelWithScale(j, i, pixels[i*core.DisplayW+j])
		}
	}
}

func drawPixelWithScale(x, y int, val bool) {
	toggle := func() color.RGBA {
		if val {
			return color.RGBA{
				R: 255,
				G: 255,
				B: 255,
				A: 255,
			}
		}
		return color.RGBA{
			R: 0,
			G: 0,
			B: 0,
			A: 255,
		}
	}()
	// Apply integer scaling
	realX := x * scalingFactor
	realY := y * scalingFactor

	head := rl.NewVector2(float32(realX), float32(realY))
	tail := rl.NewVector2(float32(realX+scalingFactor), float32(realY+scalingFactor))
	// OOB check
	if float32(realX)+tail.X > float32(core.DisplayW*scalingFactor) {
		tail.X = float32(core.DisplayW*scalingFactor - realX)
	}
	if float32(realY)+tail.Y > float32(core.DisplayH*scalingFactor)-1 {
		tail.Y = float32(core.DisplayH*scalingFactor - realY)
	}
	rl.DrawRectangleV(head, tail, toggle)
}

func checkKeys() {
	// Check for input pre-draw, event polling is done on
	// call to EndDrawing(), we want to check before next draw
	// Checking after causes display to lag behind
	// Checking all keys once per frame is the most responsive
	for code, key := range keymap {
		// Use SetKey here to let keypad track key state
		// as raylib doesn't emit events for keyDown/keyUp, unless
		// you check on per-key basis
		System.Keypad.SetKey(key, rl.IsKeyDown(code))
	}
}
