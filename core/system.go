package core

import (
	"fmt"
	"time"
)

const (
	MaxRomSize  = 3215
	SystemDelay = time.Duration(16.6667 * float32(time.Millisecond))
	CPURealHz   = 500
	CPUBaseHz   = 100
	CPUDelay    = time.Duration(float32(1.0/CPUBaseHz) * float32(time.Millisecond))
)

type System struct {
	CPU            *CPU
	ram            *CHIP8RAM
	Delay          *DelayTimer
	Display        *Display
	Sound          *SoundTimer
	Keypad         *Keypad
	systemTicker   *time.Ticker
	cpuTicker      *time.Ticker
	sysTickerClose chan struct{}
	cpuTickerClose chan struct{}
	running        bool
}

func NewCHIP8System() *System {
	sys := &System{
		CPU:     NewChip8CPU(),
		ram:     NewCHIP8RAM(),
		Display: NewChip8Display(),
		Delay:   &DelayTimer{},
		Keypad:  NewChip8Keypad(),
		Sound:   &SoundTimer{},
	}
	sys.cpuTickerClose = make(chan struct{})
	sys.sysTickerClose = make(chan struct{})
	sys.systemTicker = time.NewTicker(SystemDelay)
	sys.cpuTicker = time.NewTicker(CPUDelay)
	go func() {
		for {
			select {
			case <-sys.sysTickerClose:
				fmt.Println("Closing system ticker")
				return
			case <-sys.systemTicker.C:
				sys.Delay.tick()
				sys.Sound.tick()
			}
		}
	}()
	go func() {
		for {
			select {
			case <-sys.cpuTickerClose:
				fmt.Println("Closing CPU ticker")
				return
			case <-sys.cpuTicker.C:
				// time.Sleep/Ticker is inaccurate at millisecond/sub-millisecond intervals
				// This adds a "tick multiplier" to effectively run multiple CPU ticks per ticker event,
				// allowing ticker to run at lower, more accurate frequency.
				end := time.Now()
				stop := time.Now().Add(CPUDelay)
				ticks := 0
				// While end time not greater than next cpu base clock and # ticks not greater than max per
				// base clock
				for stop.After(end) && (ticks < CPURealHz/CPUBaseHz) {
					sys.CPU.tick()
					ticks++
					end = time.Now()
				}

			}
		}
	}()
	sys.pause()
	sys.CPU.system = sys
	return sys
}

func (c *System) ROMtoRAM(rom []byte) {
	n := len(rom)
	if n > MaxRomSize {
		panic(fmt.Errorf("ROM size: %d bytes exceeds max allowed size: %d bytes", n, MaxRomSize))
	}
	for i := 0; i < n; i++ {
		c.ram.write(RAMLowerBound+Word(i), rom[i])
	}
}

func (c *System) pause() {
	c.cpuTicker.Stop()
	c.systemTicker.Stop()
	c.running = false
}

func (c *System) unpause() {
	c.cpuTicker.Reset(CPUDelay)
	c.systemTicker.Reset(SystemDelay)
	c.running = true
}

func (c *System) ToggleRunning() {
	if c.running {
		c.pause()
		return
	}
	c.unpause()
}

func (c *System) TearDown() {
	c.sysTickerClose <- struct{}{}
	c.cpuTickerClose <- struct{}{}
}

func (c *System) Reset() {
	c.CPU.reset()
	c.Delay.value = 0
	c.Sound.value = 0
	c.cpuTicker.Reset(CPUDelay)
	c.systemTicker.Reset(SystemDelay)
}
