package core

type Keypad struct {
	keys      []bool
	awaitRecv chan<- byte
}

func NewChip8Keypad() *Keypad {
	kp := &Keypad{keys: make([]bool, 16)}
	return kp
}

func (kp *Keypad) await(receiver chan<- byte) {
	kp.awaitRecv = receiver
}

func (kp *Keypad) SetKey(k int, value bool) {
	if k < 0 || k > 15 {
		return
	}
	// If CPU is awaiting a keypress, check if this event
	// is a keyUp event, and if key state is pressed (ie. this is a rising edge)
	if kp.awaitRecv != nil && kp.keys[k] && value == false {
		kp.awaitRecv <- byte(k)
		close(kp.awaitRecv)
		kp.awaitRecv = nil
	}
	kp.keys[k] = value
}

func (kp *Keypad) getKey(k byte) bool {
	return kp.keys[k]
}
