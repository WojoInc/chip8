package core

type DisplayOperation int

const DisplayW = 64
const DisplayH = 32

type Display struct {
	grid []bool
}

func NewChip8Display() *Display {
	d := &Display{
		grid: make([]bool, DisplayW*DisplayH),
	}
	return d
}

func (d *Display) GetPixels() []bool {
	return d.grid
}

func (d *Display) drawSprite(x, y int, data []byte) bool {
	collision := false

	// Make sure no draw can start OOB
	x %= DisplayW
	y %= DisplayH

	w := 0
	h := len(data)

	// If any part of the sprite would be OOB
	// clamp it
	if x+7 > DisplayW-1 {
		w = (x + 8) - DisplayW
	}
	if y+len(data) > DisplayH {
		h = y + len(data) - DisplayH
	}
	// Loop over bytes in data
	for row := 0; row < h; row++ {
		// For each pixel
		for p := 7; p >= w; p-- {
			// Determine pixel value and location
			val := (data[row] >> p & 0x1) != 0
			loc := y*DisplayW + x + 8 - p - 1
			// Check for collision
			collision = collision || (d.grid[loc] && val)
			// Update internal pixel grid
			d.grid[loc] = d.grid[loc] != val
		}
		y++
	}
	return collision
}

func (d *Display) clear() {
	for i := range d.grid {
		d.grid[i] = false
	}
}
