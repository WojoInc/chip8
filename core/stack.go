package core

import "fmt"

type stackValue interface {
	comparable
}

type Stack[T stackValue] struct {
	top  *stackNode[T]
	size int
	cap  int
}

type stackNode[T comparable] struct {
	value T
	next  *stackNode[T]
}

func (s *Stack[T]) Push(elem T) error {
	if s.size >= s.cap {
		return &StackFullError{Cap: s.cap}
	}
	s.top = &stackNode[T]{value: elem, next: s.top}
	s.size++
	return nil
}

func (s *Stack[T]) Pop() T {
	e := s.top.value
	s.top = s.top.next
	s.size--
	return e
}

func (s *Stack[T]) Size() int {
	return s.size
}

func (s *Stack[T]) SetCap(cap int) {
	s.cap = cap
}

func (s *Stack[T]) GetCap() int {
	return s.cap
}

func NewStack[T stackValue](cap int) *Stack[T] {
	s := Stack[T]{cap: cap}
	return &s
}

type StackFullError struct {
	Cap int
}

func (s StackFullError) Error() string {
	return fmt.Sprintf("Stack full, total capacity: %d", s.Cap)
}
