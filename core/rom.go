package core

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
)

type ROM struct {
	FilePath string
	buf      bytes.Buffer
}

func (c *ROM) Size() int {
	return c.buf.Len()
}

func (c *ROM) GetBytes() []byte {
	return c.buf.Bytes()
}

func (c *ROM) Load() error {
	fp, err := filepath.Abs(c.FilePath)
	if err != nil {
		return err
	}
	f, err := os.Open(fp)
	if err != nil {
		return err
	}
	n, err := c.buf.ReadFrom(f)
	fmt.Printf("Loaded %d bytes\n", n)
	if err != nil {
		return err
	}

	return nil
}
