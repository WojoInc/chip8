package core

import (
	"fmt"
)

const StackSz = 12

type CPU struct {
	V      []byte
	I      Word
	pc     Word
	stack  *Stack[Word] // If placed in ram, should be at least C0 addresses
	iSet   []Instruction
	system *System
}

func (cpu *CPU) tick() {
	nibble, op := cpu.fetch()
	cpu.iSet[nibble](op)
}

func (cpu *CPU) fetch() (byte, Operands) {
	// Fetch opcode based on current value of program counter
	op := Word(cpu.system.ram.read(cpu.pc))<<8 | Word(cpu.system.ram.read(cpu.pc+1))
	cpu.pc += 2

	// Grab the upper nibble to use for jump table
	nibble := byte(op >> 12 & 0xF)

	// Mask off the lower 12 bits, and convert to an Operands type
	// Because instructions make use of the lower 12 bits only, this
	// saves having to mask lower 12 bits in every function, and allows the function
	// definition to explicitly state that it expects an Operands 12 bit value
	// rather than expecting the programmer to remember to mask off the lower 12
	return nibble, Operands(op & 0xFFF)
}

// initInstructionSet creates a function variable corresponding to each function of the
// Instruction set. This is needed because Go does not allow pointers to function definitions,
// so making a traditional jump/lookup table must be done at runtime
func (cpu *CPU) initInstructionSet() {
	cpu.iSet = make([]Instruction, 16)
	cpu.iSet[0] = cpu.opGrp0
	cpu.iSet[1] = cpu.opGrp1
	cpu.iSet[2] = cpu.opGrp2
	cpu.iSet[3] = cpu.opGrp3
	cpu.iSet[4] = cpu.opGrp4
	cpu.iSet[5] = cpu.opGrp5
	cpu.iSet[6] = cpu.opGrp6
	cpu.iSet[7] = cpu.opGrp7
	cpu.iSet[8] = cpu.opGrp8
	cpu.iSet[9] = cpu.opGrp9
	cpu.iSet[10] = cpu.opGrpA
	cpu.iSet[11] = cpu.opGrpB
	cpu.iSet[12] = cpu.opGrpC
	cpu.iSet[13] = cpu.opGrpD
	cpu.iSet[14] = cpu.opGrpE
	cpu.iSet[15] = cpu.opGrpF
}

func (cpu *CPU) reset() {
	cpu.V = make([]byte, 16)
	cpu.stack = NewStack[Word](StackSz)
	cpu.pc = RAMLowerBound
	cpu.I = 0x0
}

func (cpu *CPU) DumpStack() string {
	output := ""
	for i := range StackSz {
		output += fmt.Sprintf("0x%.2X: %.2x\n", i, 0x0)
	}
	return output
}

// NewChip8CPU creates a new instance of CPU and returns the
// reference
func NewChip8CPU() *CPU {
	cpu := CPU{}
	cpu.initInstructionSet()
	cpu.reset()
	return &cpu
}

func toBcd(v byte) Word {
	// double-dabble algorithm
	// Cheating a bit since we know v is always 8 bits

	// Allocate an uint, even though we're only going to use 20 bits
	var bcd = uint(v)
	for i := 0; i < 8; i++ {
		// Check ones digit
		if bcd>>8&0xF >= 0x5 {
			bcd += 3 << 8
		}
		// Check tens digit
		if bcd>>12&0xF >= 0x5 {
			bcd += 3 << 12
		}
		// Check hundreds digit
		if bcd>>16&0xF >= 0x5 {
			bcd += 3 << 16
		}
		bcd = bcd << 1
	}
	return Word(bcd >> 8)
}
