package ui

import (
	rl "github.com/gen2brain/raylib-go/raylib"
	"math"
)

var (
	nSamples          = 48000 * 10
	frequency float32 = 200
	as        rl.AudioStream
	sineIdx   float32 = 0.0
	loudness          = 100.0
	playing           = true
)

func initAudio() {
	rl.InitAudioDevice()
	as = rl.LoadAudioStream(uint32(nSamples), 32, 1)
	rl.SetAudioStreamCallback(as, audioCallback)
	System.Sound.RegisterHook(beep)
}

func audioCallback(data []float32, frames int) {
	inc := frequency / float32(nSamples)
	for i := 0; i < frames; i++ {
		data[i] = float32(loudness * math.Sin(float64(2.0*rl.Pi*sineIdx)))
		sineIdx += inc
		if sineIdx > 1.0 {
			sineIdx -= 1.0
		}
	}
}

func beep(play bool) {
	if playing == play {
		return
	}
	if play {
		rl.PlayAudioStream(as)
	} else {
		rl.StopAudioStream(as)
	}
	playing = play
}
